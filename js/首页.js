

document.addEventListener('DOMContentLoaded', function() {
    var welcomeText = document.getElementById('welcome');
    setTimeout(function() {
      welcomeText.style.transform = 'translate(550px, 260px)';
      welcomeText.style.opacity = '0';
    }, 4000);
  });


// 轮播图片
imgs=['../img/西山1.jpg','../img/大藤峡题词.jpg','../img/东塔3.jpg','../img/白石洞天.jpg','../img/太平天国起义博物馆.jpg']
var i=0;
var currentIndex = 0;
var slideInterval;

function showSlide(index) {
  var img = document.querySelector('.show img');
  currentIndex = (index + imgs.length) % imgs.length; // 防止下标越界
  img.src = imgs[currentIndex];
}

function nextSlide() {
  showSlide(currentIndex + 1);
}

function previousSlide() {
  showSlide(currentIndex - 1);
}
function stopSlideShow() {
  clearInterval(slideInterval);
}

function startSlideShow() {
  slideInterval = setInterval(nextSlide, 2500);
}

var slideshowContainer = document.querySelector('.show');

slideshowContainer.addEventListener('mouseleave', startSlideShow);
slideshowContainer.addEventListener('mouseenter', stopSlideShow);

var prevArrow = document.querySelector('.prev');
var nextArrow = document.querySelector('.next');
prevArrow.addEventListener('click', previousSlide);
nextArrow.addEventListener('click', nextSlide);
startSlideShow(); // 自动开始轮播



var videoPlayer = document.querySelector('.myvideo video')
slideshowContainer.addEventListener('click', function() {
  videoPlayer.play();
});