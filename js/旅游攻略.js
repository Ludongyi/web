var map= document.querySelector("#map");
var routeImages = document.getElementById("routeImages");
var route = document.getElementById("route");
var main = document.getElementById("main");

var coordinates = [
  { x: 317, y: 70 },
  { x: 350, y: 110 },
  { x: 322, y: 155 },
  { x: 322, y: 170 },
  { x: 360, y: 240 },
  { x: 320, y: 120 },
];

var images = [
"../img/白石山.jpg",
"../img/西山1.jpg",
"../img/东塔.jpg",
"../img/大藤峡水利1.jpg",
"../img/太平天国.jpg",
"../img/龙潭桥.jpg"
];

var names=["白石山景区","西山景区","东塔景区","大藤峡景区","太平天国景区","龙潭森林公园景区"]

function generateRoute() {
  clearMap();
  clearImages();
  main.style.display = "block"; 
  route.innerText ="生成的旅游路线如下：";
    
  // 生成随机索引序列
  var sequence = generateSequence(images.length);

  for (var i = 0; i < sequence.length; i++) {
    var imageIndex = sequence[i];
    var imageUrl = images[imageIndex];
    var coordinate = coordinates[imageIndex];

    // 显示图片
    var img = document.createElement('img');
    img.src = imageUrl;
    routeImages.appendChild(img);

    // 显示坐标点
    var dot = document.createElement('div');
        dot.className = 'dot';
        dot.style.left = coordinate.x + 'px';
        dot.style.top = coordinate.y + 'px';
        map.style.backgroundImage = "url('../img/导游图.jpg')";
        map.appendChild(dot);

    // 显示名称
    var label = document.createElement('div');
        label.className = 'label';
        label.innerText = String(i+1)+names[imageIndex];
        route.appendChild(label);

  }
}

function clearMap() {
  map.innerHTML = '';
}

function clearImages() {
  routeImages.innerHTML = '';
  route.innerHTML = '';
}

// 生成随机索引序列
function generateSequence(length) {
  var sequence = [];
  for (var i = 0; i < length; i++) {
    sequence.push(i);
  }
  sequence.sort(function () {
    return 0.5 - Math.random();
  });
  return sequence;
}


